module app.services {
    interface IDataAccessService {
        getAllDrivers(id: string): app.drivers.IDrivers[];
        getAllConstructors(): app.constructors.IConstructors[];
        getAllRaces(): app.races.IRaces[];
        getDriverDetails(id: string): app.drivers.IDriverDetails[];
        getRaceDetailsResults(id: number): app.races.IRaceDetails[];
        getTeamDetailsPosition(id: string): app.constructors.ITeamDetails[]
        
    }

    export class DataAccessService implements IDataAccessService {

        static $inject = ['$http'];
        constructor(private $http: any, public returnedData: any) {

        }

        getAllDrivers(id: string): any {
            return this.$http({
                method: 'GET',
                url: 'https://ergast.com/api/f1/'+ id +'/driverStandings.json'
            });
        }
        getDriverDetails(id: string): app.drivers.IDriverDetails[] {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/2013/drivers/' + id + '/results.json'
            });
        }
        getTeamDetailsPosition(id: string): app.constructors.ITeamDetails[] {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/2013/constructors/' + id + '/constructorStandings.json'
            });
        }
        getAllConstructors(): app.constructors.IConstructors[] {
            return this.$http({
                method: 'GET',
                url: 'https://ergast.com/api/f1/2013/constructorStandings.json'
            });
        }
        getTeamDetails(id: string): app.constructors.ITeamDetails[] {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/2013/constructors/' + id + '/results.json'
            });
        }
        getAllRaces(): app.races.IRaces[] {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/2013/results/1.json'
                
            });
        }
        getRaceDetailsQualify(id: number): app.constructors.ITeamDetails[] {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/2013/' + id + '/qualifying.json'
            });
        }
        getRaceDetailsResults(id: number): app.races.IRaceDetails[] {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/2013/' + id + '/results.json'
            });
        }
        getAllSeasons(): any {
            return this.$http({
                method: 'GET',
                url: 'http://ergast.com/api/f1/seasons.json?limit=1000'
            });
        }
        // 

        getData(url: string) {
            return this.$http.get(url).then((response: any) => {
                this.returnedData = response.data;
                return this.returnedData;
            })
        }

        
    }
    angular.module("formula").service("dataAccessService", DataAccessService);
    }