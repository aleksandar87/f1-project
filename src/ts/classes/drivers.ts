module app.drivers {
export interface IDrivers {
    position: number;
    givenName: string;
    familyName: string;
    points: number;
}
}