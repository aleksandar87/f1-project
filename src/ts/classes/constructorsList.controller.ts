module app.constructors {
    interface IConstructorList {
        title: string;
        
        constructorsList: app.constructors.IConstructors[];
    }

    class ConstructorsListCtrl implements IConstructorList {
        
        title: string;
        
        

        static $inject = ['dataAccessService'];
        constructor(private dataAccessService: app.services.DataAccessService, public constructorsList: app.constructors.IConstructors[]) {
            this.title = 'Championship 2013';
            

            const productResourcePromise: any = this.dataAccessService.getAllConstructors();
            productResourcePromise.then((result: { data: { MRData: { StandingsTable: { StandingsLists: { ConstructorStandings: any; }[]; }; }; }; }) => {
                const data = result.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
                this.constructorsList = data;
            });
        }
    }

    
    angular.module('formula')
        .controller("ConstructorsListCtrl", ConstructorsListCtrl);
}
