module app.drivers {
    interface IDriverList {
        driverDetail: app.drivers.IDriverDetails[];
    }
    interface IRouteParams extends ng.route.IRouteParamsService {
        driverId: string;
    }
    class DriverDetailsCtrl implements IDriverList {
        
        static $inject = ['dataAccessService', '$routeParams'];
        driverId: string;
        constructor(private dataAccessService: app.services.DataAccessService, private $routeParams: IRouteParams, public driverDetail: app.drivers.IDriverDetails[]) {
            
         this.driverId = $routeParams.driverId;  

            const productResourcePromise: any = this.dataAccessService.getDriverDetails(this.driverId);
            productResourcePromise.then((result: { data: { MRData: { RaceTable: { Races: any }; }; }; }) => {
                const data = result.data.MRData.RaceTable.Races;
                this.driverDetail = data;
                //console.log(this.racesList);
            });
        }
    }

    
    angular.module('formula')
        .controller("DriverDetailsCtrl", DriverDetailsCtrl);
}
