module app.drivers {
    interface IDriverList {
        title: string;
        
        driversList: app.drivers.IDrivers[];
    }

    class DriversListCtrl implements IDriverList {
        
        title: string;
        
        

        static $inject = ['dataAccessService'];
        constructor(private dataAccessService: app.services.DataAccessService, public newData: any, public driversList: app.drivers.IDrivers[], public seasonList: any, public selectedValue: string) {
            this.title = 'Championship 2013';
            this.selectedValue = '2013';
            

            const productResourcePromise: any = this.dataAccessService.getAllDrivers(this.selectedValue);
            productResourcePromise.then((result: { data: { MRData: { StandingsTable: { StandingsLists: { DriverStandings: any; }[]; }; }; }; }) => {
                const data = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                this.driversList = data;
                console.log(this.driversList[0]);
            });

            const seasons: any = this.dataAccessService.getAllSeasons();
            seasons.then((result: { data: { MRData: { SeasonTable: { Seasons: any}; }; }; }) => {
                const data = result.data.MRData.SeasonTable.Seasons;
                this.seasonList = data;
                console.log(this.seasonList);
            });

            
            
        }

        getData(id: string) {
            console.log(`Ovde sam ${id}`);
            
            /*this.dataAccessService.getAllDrivers(id).then((result: { data: { MRData: { StandingsTable: { StandingsLists: { DriverStandings: any; }[]; }; }; }; }) => {
                const data = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                this.driversList = data;
                
                
            })*/
            // this.newData.then((result: { data: { MRData: { StandingsTable: { StandingsLists: { DriverStandings: any; }[]; }; }; }; }) => {
            //     const data = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;

            //     return this.driversList = data;
            //     // console.log(this.driversList[0]);
            // });
        }

        // getDrivers(): any {   
        //     this.drivers = this.drivers.$$state.value.MRData.StandingsTable.StandingsLists[0].DriverStandings;
        //     return this.drivers;
        // }

    }

    
    angular.module('formula')
        .controller("DriversListCtrl", DriversListCtrl);
}
