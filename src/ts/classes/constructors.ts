module app.constructors {

    export interface IConstructors {
        position: number;
        name: string;
        details: string;
        points: number;
    }


}