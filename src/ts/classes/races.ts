module app.races {
    export interface IRaces {
        round: number;
        raceName: string;
        circuitName: string;
        date: string;
        winner: string;
    }
}