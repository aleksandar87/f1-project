module app.races {
    interface IRaceList {
        raceDetailQ: app.races.IRaceDetails[];
        raceDetailR: app.races.IRaceDetails[]
    }
    interface IRouteParams extends ng.route.IRouteParamsService {
        round: number;
    }
    class RaceDetailsCtrl implements IRaceList {
        
        
        static $inject = ['dataAccessService', '$routeParams'];
        round: number;
        constructor(private dataAccessService: app.services.DataAccessService, private $routeParams: IRouteParams, public raceDetailQ: app.races.IRaceDetails[], public raceDetailR: app.races.IRaceDetails[]) {
            
         this.round = $routeParams.round;  

            const productResourcePromise: any = this.dataAccessService.getRaceDetailsQualify(this.round);
            productResourcePromise.then((result: { data: { MRData: { RaceTable: { Races: any }; }; }; }) => {
                const data = result.data.MRData.RaceTable.Races;
                this.raceDetailQ = data;
                //console.log(this.racesList);
            });
            const productResourcePromiseR: any = this.dataAccessService.getRaceDetailsResults(this.round);
            productResourcePromiseR.then((result: { data: { MRData: { RaceTable: { Races: any }; }; }; }) => {
                const data = result.data.MRData.RaceTable.Races;
                this.raceDetailR = data;
                //console.log(this.racesList);
            });
        }
    }

    
    angular.module('formula')
        .controller("RaceDetailsCtrl", RaceDetailsCtrl);
}
