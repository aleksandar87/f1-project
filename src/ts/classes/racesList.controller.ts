module app.races {
    interface IRaceList {
        title: string;
        
        racesList: app.races.IRaces[];
    }

    class RacesListCtrl implements IRaceList {
        
        title: string;
        
        

        static $inject = ['dataAccessService'];
        constructor(private dataAccessService: app.services.DataAccessService, public racesList: app.races.IRaces[]) {
            this.title = 'Championship 2013';
            

            const productResourcePromise: any = this.dataAccessService.getAllRaces();
            productResourcePromise.then((result: { data: { MRData: { RaceTable: { Races: any }; }; }; }) => {
                const data = result.data.MRData.RaceTable.Races;
                this.racesList = data;
                //console.log(this.racesList);
            });
        }
    }

    
    angular.module('formula')
        .controller("RacesListCtrl", RacesListCtrl);
}
