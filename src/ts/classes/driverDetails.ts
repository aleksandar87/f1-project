module app.drivers {
    export interface IDriverDetails {
        givenName: string;
        familiyName: string;
        dateOfBirth: string;
        nationality: string;
    }
}