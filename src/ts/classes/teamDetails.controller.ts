module app.constructors {
    interface ITeamList {
        teamDetail: app.constructors.ITeamDetails[];
    }
    interface IRouteParams extends ng.route.IRouteParamsService {
        constructorId: string;
    }
    class TeamDetailsCtrl implements ITeamList {
        
        static $inject = ['dataAccessService', '$routeParams'];
        constructorId: string;
        constructor(private dataAccessService: app.services.DataAccessService, private $routeParams: IRouteParams, public teamDetail: app.constructors.ITeamDetails[], public teamDetailPosition: app.constructors.ITeamDetails[]) {
            
         this.constructorId = $routeParams.constructorId;  

            const productResourcePromise: any = this.dataAccessService.getTeamDetails(this.constructorId);
            productResourcePromise.then((result: { data: { MRData: { RaceTable: { Races: any }; }; }; }) => {
                const data = result.data.MRData.RaceTable.Races;
                this.teamDetail = data;
                //console.log(this.racesList);
            });
            const pointsAndPosition: any = this.dataAccessService.getTeamDetailsPosition(this.constructorId);
            pointsAndPosition.then((result: { data: { MRData: { StandingsTable: { StandingsLists: { ConstructorStandings: any; }[]; }; }; }; }) => {
                const data = result.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
                this.teamDetailPosition = data;
            });
        }
        
    }

    
    angular.module('formula')
        .controller("TeamDetailsCtrl", TeamDetailsCtrl);
}
