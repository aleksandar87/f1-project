"use strict";
var app;
(function (app) {
    var constructors;
    (function (constructors) {
        var TeamDetailsCtrl = /** @class */ (function () {
            function TeamDetailsCtrl(dataAccessService, $routeParams, teamDetail, teamDetailPosition) {
                var _this = this;
                this.dataAccessService = dataAccessService;
                this.$routeParams = $routeParams;
                this.teamDetail = teamDetail;
                this.teamDetailPosition = teamDetailPosition;
                this.constructorId = $routeParams.constructorId;
                var productResourcePromise = this.dataAccessService.getTeamDetails(this.constructorId);
                productResourcePromise.then(function (result) {
                    var data = result.data.MRData.RaceTable.Races;
                    _this.teamDetail = data;
                    //console.log(this.racesList);
                });
                var pointsAndPosition = this.dataAccessService.getTeamDetailsPosition(this.constructorId);
                pointsAndPosition.then(function (result) {
                    var data = result.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
                    _this.teamDetailPosition = data;
                });
            }
            TeamDetailsCtrl.$inject = ['dataAccessService', '$routeParams'];
            return TeamDetailsCtrl;
        }());
        angular.module('formula')
            .controller("TeamDetailsCtrl", TeamDetailsCtrl);
    })(constructors = app.constructors || (app.constructors = {}));
})(app || (app = {}));

//# sourceMappingURL=teamDetails.controller.js.map
