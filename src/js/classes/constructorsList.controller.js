"use strict";
var app;
(function (app) {
    var constructors;
    (function (constructors) {
        var ConstructorsListCtrl = /** @class */ (function () {
            function ConstructorsListCtrl(dataAccessService, constructorsList) {
                var _this = this;
                this.dataAccessService = dataAccessService;
                this.constructorsList = constructorsList;
                this.title = 'Championship 2013';
                var productResourcePromise = this.dataAccessService.getAllConstructors();
                productResourcePromise.then(function (result) {
                    var data = result.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
                    _this.constructorsList = data;
                });
            }
            ConstructorsListCtrl.$inject = ['dataAccessService'];
            return ConstructorsListCtrl;
        }());
        angular.module('formula')
            .controller("ConstructorsListCtrl", ConstructorsListCtrl);
    })(constructors = app.constructors || (app.constructors = {}));
})(app || (app = {}));

//# sourceMappingURL=constructorsList.controller.js.map
