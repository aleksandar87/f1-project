"use strict";
var app;
(function (app) {
    var drivers;
    (function (drivers) {
        var DriversListCtrl = /** @class */ (function () {
            function DriversListCtrl(dataAccessService, newData, driversList, seasonList, selectedValue) {
                var _this = this;
                this.dataAccessService = dataAccessService;
                this.newData = newData;
                this.driversList = driversList;
                this.seasonList = seasonList;
                this.selectedValue = selectedValue;
                this.title = 'Championship 2013';
                this.selectedValue = '2013';
                var productResourcePromise = this.dataAccessService.getAllDrivers(this.selectedValue);
                productResourcePromise.then(function (result) {
                    var data = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                    _this.driversList = data;
                    console.log(_this.driversList[0]);
                });
                var seasons = this.dataAccessService.getAllSeasons();
                seasons.then(function (result) {
                    var data = result.data.MRData.SeasonTable.Seasons;
                    _this.seasonList = data;
                    console.log(_this.seasonList);
                });
            }
            DriversListCtrl.prototype.getData = function (id) {
                console.log("Ovde sam " + id);
                /*this.dataAccessService.getAllDrivers(id).then((result: { data: { MRData: { StandingsTable: { StandingsLists: { DriverStandings: any; }[]; }; }; }; }) => {
                    const data = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                    this.driversList = data;
                    
                    
                })*/
                // this.newData.then((result: { data: { MRData: { StandingsTable: { StandingsLists: { DriverStandings: any; }[]; }; }; }; }) => {
                //     const data = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                //     return this.driversList = data;
                //     // console.log(this.driversList[0]);
                // });
            };
            DriversListCtrl.$inject = ['dataAccessService'];
            return DriversListCtrl;
        }());
        angular.module('formula')
            .controller("DriversListCtrl", DriversListCtrl);
    })(drivers = app.drivers || (app.drivers = {}));
})(app || (app = {}));

//# sourceMappingURL=driversList.controller.js.map
