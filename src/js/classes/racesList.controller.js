"use strict";
var app;
(function (app) {
    var races;
    (function (races) {
        var RacesListCtrl = /** @class */ (function () {
            function RacesListCtrl(dataAccessService, racesList) {
                var _this = this;
                this.dataAccessService = dataAccessService;
                this.racesList = racesList;
                this.title = 'Championship 2013';
                var productResourcePromise = this.dataAccessService.getAllRaces();
                productResourcePromise.then(function (result) {
                    var data = result.data.MRData.RaceTable.Races;
                    _this.racesList = data;
                    //console.log(this.racesList);
                });
            }
            RacesListCtrl.$inject = ['dataAccessService'];
            return RacesListCtrl;
        }());
        angular.module('formula')
            .controller("RacesListCtrl", RacesListCtrl);
    })(races = app.races || (app.races = {}));
})(app || (app = {}));

//# sourceMappingURL=racesList.controller.js.map
