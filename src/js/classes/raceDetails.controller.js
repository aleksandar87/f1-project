"use strict";
var app;
(function (app) {
    var races;
    (function (races) {
        var RaceDetailsCtrl = /** @class */ (function () {
            function RaceDetailsCtrl(dataAccessService, $routeParams, raceDetailQ, raceDetailR) {
                var _this = this;
                this.dataAccessService = dataAccessService;
                this.$routeParams = $routeParams;
                this.raceDetailQ = raceDetailQ;
                this.raceDetailR = raceDetailR;
                this.round = $routeParams.round;
                var productResourcePromise = this.dataAccessService.getRaceDetailsQualify(this.round);
                productResourcePromise.then(function (result) {
                    var data = result.data.MRData.RaceTable.Races;
                    _this.raceDetailQ = data;
                    //console.log(this.racesList);
                });
                var productResourcePromiseR = this.dataAccessService.getRaceDetailsResults(this.round);
                productResourcePromiseR.then(function (result) {
                    var data = result.data.MRData.RaceTable.Races;
                    _this.raceDetailR = data;
                    //console.log(this.racesList);
                });
            }
            RaceDetailsCtrl.$inject = ['dataAccessService', '$routeParams'];
            return RaceDetailsCtrl;
        }());
        angular.module('formula')
            .controller("RaceDetailsCtrl", RaceDetailsCtrl);
    })(races = app.races || (app.races = {}));
})(app || (app = {}));

//# sourceMappingURL=raceDetails.controller.js.map
