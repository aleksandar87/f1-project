"use strict";
var app;
(function (app) {
    var drivers;
    (function (drivers) {
        var DriverDetailsCtrl = /** @class */ (function () {
            function DriverDetailsCtrl(dataAccessService, $routeParams, driverDetail) {
                var _this = this;
                this.dataAccessService = dataAccessService;
                this.$routeParams = $routeParams;
                this.driverDetail = driverDetail;
                this.driverId = $routeParams.driverId;
                var productResourcePromise = this.dataAccessService.getDriverDetails(this.driverId);
                productResourcePromise.then(function (result) {
                    var data = result.data.MRData.RaceTable.Races;
                    _this.driverDetail = data;
                    //console.log(this.racesList);
                });
            }
            DriverDetailsCtrl.$inject = ['dataAccessService', '$routeParams'];
            return DriverDetailsCtrl;
        }());
        angular.module('formula')
            .controller("DriverDetailsCtrl", DriverDetailsCtrl);
    })(drivers = app.drivers || (app.drivers = {}));
})(app || (app = {}));

//# sourceMappingURL=driverDetails.controller.js.map
