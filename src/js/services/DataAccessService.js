"use strict";
var app;
(function (app) {
    var services;
    (function (services) {
        var DataAccessService = /** @class */ (function () {
            function DataAccessService($http, returnedData) {
                this.$http = $http;
                this.returnedData = returnedData;
            }
            DataAccessService.prototype.getAllDrivers = function (id) {
                return this.$http({
                    method: 'GET',
                    url: 'https://ergast.com/api/f1/' + id + '/driverStandings.json'
                });
            };
            DataAccessService.prototype.getDriverDetails = function (id) {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/2013/drivers/' + id + '/results.json'
                });
            };
            DataAccessService.prototype.getTeamDetailsPosition = function (id) {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/2013/constructors/' + id + '/constructorStandings.json'
                });
            };
            DataAccessService.prototype.getAllConstructors = function () {
                return this.$http({
                    method: 'GET',
                    url: 'https://ergast.com/api/f1/2013/constructorStandings.json'
                });
            };
            DataAccessService.prototype.getTeamDetails = function (id) {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/2013/constructors/' + id + '/results.json'
                });
            };
            DataAccessService.prototype.getAllRaces = function () {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/2013/results/1.json'
                });
            };
            DataAccessService.prototype.getRaceDetailsQualify = function (id) {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/2013/' + id + '/qualifying.json'
                });
            };
            DataAccessService.prototype.getRaceDetailsResults = function (id) {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/2013/' + id + '/results.json'
                });
            };
            DataAccessService.prototype.getAllSeasons = function () {
                return this.$http({
                    method: 'GET',
                    url: 'http://ergast.com/api/f1/seasons.json?limit=1000'
                });
            };
            // 
            DataAccessService.prototype.getData = function (url) {
                var _this = this;
                return this.$http.get(url).then(function (response) {
                    _this.returnedData = response.data;
                    return _this.returnedData;
                });
            };
            DataAccessService.$inject = ['$http'];
            return DataAccessService;
        }());
        services.DataAccessService = DataAccessService;
        angular.module("formula").service("dataAccessService", DataAccessService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));

//# sourceMappingURL=DataAccessService.js.map
