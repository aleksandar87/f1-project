"use strict";
var app;
(function (app) {
    var main = angular.module('formula', ['ngRoute', 'ngAnimate']);
    main.config(routeConfig);
    routeConfig.$inject = ['$routeProvider'];
    function routeConfig($routeProvider) {
        $routeProvider
            .when('/drivers', {
            templateUrl: './prod/views/drivers.html',
            controller: 'DriversListCtrl as vm'
        })
            .when('/drivers/:driverId', {
            templateUrl: './prod/views/driverDetails.html',
            controller: 'DriverDetailsCtrl as vm'
        })
            .when('/teams', {
            templateUrl: './prod/views/teams.html',
            controller: 'ConstructorsListCtrl as vm'
        })
            .when('/teams/:constructorId', {
            templateUrl: './prod/views/teamDetails.html',
            controller: 'TeamDetailsCtrl as vm'
        })
            .when('/races', {
            templateUrl: './prod/views/races.html',
            controller: 'RacesListCtrl as vm'
        })
            .when('/races/:round', {
            templateUrl: './prod/views/raceDetails.html',
            controller: 'RaceDetailsCtrl as vm'
        })
            .otherwise({
            redirectTo: '/drivers'
        });
    }
})(app || (app = {}));

//# sourceMappingURL=app.js.map
