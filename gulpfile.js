const gulp = require('gulp');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('gulp-browserify');
const sass = require("gulp-sass");
const htmlmin = require('gulp-htmlmin');
// const imagemin = require('gulp-imagemin');
 
gulp.task('compile-ts', function () {
    var tsProject = ts.createProject('tsconfig.json');
    var tsResult = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject())

    return tsResult.js
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(tsProject.options.outDir));
});

gulp.task("sass", function() {
    return gulp.src("./src/scss/*.scss")
     .pipe(sass())
     .pipe(gulp.dest("./prod/css"));
});

gulp.task('pages', () => {
    return gulp.src('./src/views/*.html')
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest('./prod/views'));
});

gulp.task("scripts", function(done) {
    gulp.src("./src/js/*.js")
    .pipe(browserify({
        insertGlobals : true
      }))
    .pipe(gulp.dest('./prod/js'), done());
});

// gulp.task('images', () =>
//     gulp.src('./src/images/**/**')
//         .pipe(imagemin())
//         .pipe(gulp.dest('./prod/images'))
// );

gulp.task("default", gulp.series("compile-ts", "scripts", "pages", "sass"));

gulp.task("watch:all", function() {
    return gulp.watch(["./src/**/*.ts", "./src/**/*.scss", "./src/**/*.html"], gulp.series("default"));
});